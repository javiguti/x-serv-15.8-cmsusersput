from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.show_content, name='show_content'),
    path('logout/', views.logout_view, name='logout_view'),
    path('<str:name>', views.cms_post, name='cmsuserput')
]