from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.template import loader
from .models import MyFirstApp
from .forms import ContentForm
from django.contrib.auth import logout

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

def index(request):

    pages = MyFirstApp.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return HttpResponse(html_template.format(body=body))

@csrf_exempt
def cms_post(request, name):
    if request.user.is_authenticated:
        if request.method == 'PUT':
            try:
                p = MyFirstApp.objects.get(name=name)
            except MyFirstApp.DoesNotExist:
                p = MyFirstApp(name=name)
            p.content = request.body.decode("utf-8")
            p.save()

        if request.method == "POST":
            try:
                p = MyFirstApp.objects.get(name=name)
            except MyFirstApp.DoesNotExist:
                p = MyFirstApp(name=name)
            form = ContentForm(request.POST)
            if form.is_valid():
                p.content = form.cleaned_data['content']
                p.save()

        if request.method == 'GET' or request.method == 'PUT' or request.method == 'POST':
            try:
                p = MyFirstApp.objects.get(name=name)
                content_form = ContentForm(initial={'content': p.content})
                status = 200
            except MyFirstApp.DoesNotExist:
                content = "Page " + name + " not found."
                content_form = ContentForm()
                status = 404
            content_template = loader.get_template('pages/content.html')
            content_html = content_template.render({'page': name,
                                                'form': content_form},
                                               request)
            return (HttpResponse(content_html, status=status))
    else:
        if request.method == 'GET' or request.method == 'POST' or request.method == 'PUT':
            try:
                p = MyFirstApp.objects.get(name=name)
                content_form = ContentForm(initial={'content': p.content})
                status = 200
                content_template = loader.get_template('pages/content.html')
                content_html = content_template.render({'page': name,
                                                    'form': content_form},
                                                   request)
                return (HttpResponse(content_html, status=status))
            except MyFirstApp.DoesNotExist:
                    logged = 'Not logged in. You cant modificate or see contents. <a href="/admin/"> Login </a>'
                    return HttpResponse(logged)

def show_content(request):
    if request.user.is_authenticated:
        logged = 'Logged in as ' + request.user.username + ' <a href="/logout/"> Logout </a>'
    else:
        logged = 'Not logged in. <a href="/admin/"> Login </a>'
    return HttpResponse(logged)

def logout_view(request):
    logout(request)
    return redirect("/")