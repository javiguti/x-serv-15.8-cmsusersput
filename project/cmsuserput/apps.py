from django.apps import AppConfig


class CmsPostConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cmsuserput'
