from django.db import models

# Create your models here.

class MyFirstApp(models.Model):
    name = models.CharField(max_length=256)
    content = models.TextField()
